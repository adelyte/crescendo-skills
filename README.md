Follow the instructions to master each Crescendo skill

----
## Skill: Switcher Input Configuration
Switchers 61 and 71 are missing their input configuration.
Add Source IDs to the input parameters, using format `\x01`, to make them work.

Use the R-01 signals in SIMPL Debugger to confirm each Switcher responds to incoming source selections correctly, according to the documentation below.


<br>
#### Configuration 1:

Display Inputs

* Apple TV and Game are both HDMI 1

Receiver Inputs

* Apple TV / AirPlay: Video Aux *(AirPlay is an audio-only Source of the Apple TV)*
* Game: Game
* Sonos: CD

<br>    
#### Configuration 2:

Display Inputs

* Apple TV and Game are both HDMI 1
* Sonos and AirPlay should power off the display

Receiver Inputs

* Apple TV / AirPlay: BD
* Game: Game
* Sonos: CD
    

<br>  
#### Configuration 3:

Display Inputs

* Apple TV: HDMI 1
* Game: HDMI 2
* Sonos should power off the display
* AirPlay should not affect the display

<br>
The receiver is malfunctioning. A replacement has been ordered, but a temporary programming change is needed for the weekend.
Leave the programming for the receiver in tact.

Remove the receiver `\x61` from the Living Room **Room** module.

Set display volume to `Relative`